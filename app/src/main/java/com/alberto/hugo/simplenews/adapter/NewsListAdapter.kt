package com.alberto.hugo.simplenews.adapter

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.alberto.hugo.simplenews.DetailActivity
import com.alberto.hugo.simplenews.DetailActivity.Companion.NEWS_ITEM
import com.alberto.hugo.simplenews.R
import com.alberto.hugo.simplenews.databinding.RowNewsItemBinding
import com.alberto.hugo.simplenews.model.News
import com.alberto.hugo.simplenews.model.NewsView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import java.util.ArrayList

class NewsListAdapter(private val context: Context) : RecyclerView.Adapter<NewsListAdapter.ViewHolder>() {

    private var mNewsList = ArrayList<News>()

    inner class ViewHolder internal constructor(val binding: RowNewsItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        init {
            binding.card.setOnClickListener {
                context.startActivity(
                        Intent(context, DetailActivity::class.java).apply {
                            putExtra(NEWS_ITEM, Gson().toJson(mNewsList[adapterPosition]))
                            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        })
            }
        }

        fun bind(newsView: NewsView) {
            binding.data = newsView
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.row_news_item, parent, false)!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mNewsList[position]
        holder.bind(NewsView(model))

        // Load the article image
        Picasso.with(context)
                .load(model.image.url)
                .fit()
                .centerCrop()
                .error(R.drawable.ic_insert_photo_black_24dp)
                .into(holder.binding.image)
    }

    override fun getItemCount(): Int {
        return mNewsList.size
    }

    fun addItem(news: News) {
        mNewsList.add(news)
        notifyItemInserted(mNewsList.size-1)
    }

    fun removeItems() {
        mNewsList.clear()
        notifyDataSetChanged()
    }
}