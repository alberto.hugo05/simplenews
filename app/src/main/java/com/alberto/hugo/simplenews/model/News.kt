package com.alberto.hugo.simplenews.model

import android.databinding.BaseObservable
import android.databinding.Bindable
import com.google.gson.annotations.SerializedName

data class News (
        @SerializedName("pubDate") var date: String,
        @SerializedName("enclosure") var image: Image,
        @SerializedName("title") var title: String,
        @SerializedName("description") var content: String,
        @SerializedName("link") var link: String)

data class Image (
        @SerializedName("length") var length: String,
        @SerializedName("type") var type: String,
        @SerializedName("url") var url: String)

class NewsView (private val news: News) : BaseObservable() {

    @Bindable fun getTitle(): String { return news.title }
    @Bindable fun getDate(): String { return news.date }
    @Bindable fun getContent(): String { return news.content }
}