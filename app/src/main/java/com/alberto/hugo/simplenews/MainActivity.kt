package com.alberto.hugo.simplenews

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import com.alberto.hugo.simplenews.adapter.NewsListAdapter
import com.alberto.hugo.simplenews.model.News
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import org.jetbrains.anko.doAsync
import java.io.IOException
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import android.support.v7.widget.DividerItemDecoration
import android.view.View

class MainActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {

    private val mAdapter by lazy { NewsListAdapter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Init recycler view layout & adapter
        swipe_container.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent))
        swipe_container.setOnRefreshListener(this)
        (main_recycler.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        val layoutManager = LinearLayoutManager(this).apply { isAutoMeasureEnabled = true }
        main_recycler.layoutManager = layoutManager
        main_recycler.adapter = mAdapter
        main_recycler.addItemDecoration(DividerItemDecoration(this, layoutManager.orientation))

        requestNewsWebsiteAsync(NEWS_URL)
    }

    /*
    On swipe_container refresh
     */
    override fun onRefresh() {
        requestNewsWebsiteAsync(NEWS_URL)
    }

    /**
     * Request [url] address and return it in [getListRequestCallback] callback
     * @param url http(s) address of the RSS
     */
    private fun requestNewsWebsiteAsync(url: String) {
        doAsync {
            OkHttpClient()
                    .newCall(Request.Builder().url(url).build())
                    .enqueue(getListRequestCallback)
        }
    }

    private val getListRequestCallback = object : Callback {
        /**
         * When no connectivity, empty adapter and show informative layout
         */
        override fun onFailure(call: Call?, e: IOException?) {
            runOnUiThread {
                swipe_container.isRefreshing = false
                mAdapter.removeItems()
                no_connectivity_layout.visibility = View.VISIBLE
            }
        }

        /**
         * Retreive body xml response and convert it to json
         * Then populate the adapter with the [News] items
         */
        override fun onResponse(call: Call?, response: Response?) {
            runOnUiThread {
                swipe_container.isRefreshing = false
                mAdapter.removeItems()
                no_connectivity_layout.visibility = View.GONE
            }
            val body = response?.body()?.string()
            if (body == null) {
                Snackbar.make(main_view, getString(R.string.error_converting), Snackbar.LENGTH_LONG)
                return
            }
            val xmlToJson = XmlToJson.Builder(body).build()
            val items = xmlToJson.toJson()
                    ?.optJSONObject("rss")
                    ?.optJSONObject("channel")
                    ?.getJSONArray("item")
            if (items == null) {
                Snackbar.make(main_view, getString(R.string.error_converting), Snackbar.LENGTH_LONG)
                return
            }
            val arraySize = items.length()
            (0 until arraySize)
                    .map { Gson().fromJson(items.getString(it), News::class.java) }
                    .forEach { runOnUiThread { mAdapter.addItem(it) } }
        }
    }

    companion object {
        const val NEWS_URL: String = "http://www.lemonde.fr/rss/une.xml"
    }
}