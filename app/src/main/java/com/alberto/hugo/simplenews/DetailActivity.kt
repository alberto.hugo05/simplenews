package com.alberto.hugo.simplenews

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.alberto.hugo.simplenews.databinding.ActivityDetailBinding
import com.alberto.hugo.simplenews.model.News
import com.alberto.hugo.simplenews.model.NewsView
import com.google.gson.Gson
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.activity_detail.*
import android.content.Intent
import android.net.Uri

class DetailActivity : AppCompatActivity() {

    private val binding: ActivityDetailBinding by lazy {
        DataBindingUtil.setContentView<ActivityDetailBinding>(this, R.layout.activity_detail)!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding
        val news = Gson().fromJson(intent.getStringExtra(NEWS_ITEM), News::class.java)
        binding.data = NewsView(news)

        // On more clicked, open the article in web page
        show_more.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW).apply { data = Uri.parse(news.link) })
        }

        // Load the article image
        Picasso.with(this)
                .load(news.image.url)
                .fit()
                .centerCrop()
                .error(R.drawable.ic_insert_photo_black_24dp)
                .into(image)
    }

    companion object {
        const val NEWS_ITEM: String = "news_item"
    }
}
