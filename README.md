# SimpleNews

Développement d'une application démo de récupération de flux RSS

## Informations

Durée de développement : 2h30 (De la création du repo à la génération de l'apk)

Durée pour le README : 40'

Réalisation d'une application Android simple, comportant 2 Activity, l'une faisant appel à l'autre par le biais d'un Intent comportant les informations de l'article cliqué.

La première Activity, que l'on va appeler 'Home' contient la liste des dernières actualités provenant du flux RSS de [Le Monde](http://www.lemonde.fr/rss/une.xml).
La deuxième, 'Detail', affiche les informations complètes de l'article cliqué depuis 'Home' avec un lien qui redirige vers l'article du site Internet.

Le but pour moi de l'exercice est de faire une base simple et lisible qui laisse place aussi à des évolutions.

La récupération du flux est faite avec OkHttp, le xml reçu est ensuite converti en Json, format beaucoup plus malléable et rependu.
Le Json est 'converti' en data class à l'aide de Gson et est affiché avec du databinding (permet un affichage plus lisible coté Kotlin).

Si l'utilisateur ne dispose pas d'internet ou si la page s'est mal téléchargé, celui-ci aura une page lui indiquant.

Développement et tests réalisés sur un Samsung S8 avec Android O.

### Problèmes identités

- Le changement du modèle du XML, aussi évident que ça parait, si celui-ci ne dispose plus du même format, il sera impossible d'afficher le flux. (Affichage d'un message Snackbar si c'est le cas)
- La rotation d'écran n'est pas géré, provoquant un rechargement inutile de la liste puisque l'utilisateur dispose déjà des informations. (Optimisation réseau)

### Évolutions possibles

- Ajouts de tests qui s'assurent à chaque déploiement que la source est toujours valide et disponible
- Mise en place de plusieurs sources RSS avec comme possibilité des Fragments contenant une liste des articles de chacun
- Un mode nuit, c'est toujours utile pour une application qui contient beaucoup de lecture
- Ajout de plusieurs type d'affichage de liste, plus ou moins grand selon le choix de l'utilisateur

### Librairies utilisés
- Kotlin for Android
- Android design support
- [XmlToJson](https://github.com/smart-fun/XmlToJson) - Conversion XML en Json (Apache-2.0)
- [Gson](https://github.com/google/gson) - Conversion Json en POJO (Apache-2.0)
- [Okhttp](https://github.com/square/okhttp) - Networking (Apache-2.0)
- [Picasso](https://github.com/square/okhttp) - Téléchargement et cache d'images (Apache-2.0)
- [Anko](https://github.com/Kotlin/anko) - Utilisation de la partie common pour disposer des taches Asynchrones (Apache-2.0)

## APK de l'application

[Téléchargement](https://gitlab.com/alberto.hugo05/simplenews/blob/master/app/release/app-release.apk) (1.28Mo)

## Captures d'écrans
<img src="https://gitlab.com/alberto.hugo05/simplenews/raw/master/app/home.png" width="300">
<img src="https://gitlab.com/alberto.hugo05/simplenews/raw/master/app/detail.png" width="300">